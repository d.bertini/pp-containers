
#sbatch --nodes 16 --tasks-per-node 32 --ntasks-per-core 1 --cpus-per-task 1 --no-requeue --job-name osu_j --mem-per-cpu 4000 --mail-type ALL --mail-user d.bertini@gsi.de --partition long --time 0-08:00:00 -D ./data -o %j.out.log -e %j.err.log  -- ./run-file.sh

# Infiniband interconnect tests
#sbatch --nodes 2 --tasks-per-node 1  --no-requeue --job-name osu_j --mail-type ALL --mail-user d.bertini@gsi.de --partition debug --time 0-00:30:00 -D ./data -o %j.out.log -e %j.err.log  -- ./run-file.sh

# Submission example for EPOCH
#sbatch --ntasks 256  --no-requeue --job-name cont_t --mem-per-cpu 4000 --mail-type ALL --mail-user d.bertini@gsi.de --partition long --time 7-00:00:00 -D ./data -o %j.out.log -e %j.err.log  -- ./run-file.sh

# Submission for WaprX:  number of tasks should be 80 (defined from input deck) 
sbatch --ntasks 80 --no-requeue --job-name warpx_mpi  --mail-type ALL --mail-user d.bertini@gsi.de --partition long --time 0-8:00:00 -D ./ -o %j.out.log -e %j.err.log   -- ./run-file.sh

