#!/bin/bash
export OMP_NUM_THREADS=1

export CONT=/lustre/rz/dbertini/containers/dev/rlx8_ompi5_ucx.sif
export OMPI_MCA_io=romio341

#export CONT=/lustre/rz/dbertini/containers/prod/rlx8_ompi_ucx.sif
#export OMPI_MCA_io=romio321

echo "." | srun --export=ALL $CONT epoch3d

