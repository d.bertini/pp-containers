#!/bin/bash

export APPTAINER_CONFIGDIR=/lustre/rz/dbertini/apptainer 
export CONT=/lustre/rz/dbertini/containers/dev/rlx8_ompi5_ucx_cma.sif

export OMP_NUM_THREADS=1
export OMPI_MCA_io=romio341

export EPOCH_EXE=epoch3d

nb_instances=`flock -x /tmp apptainer instance list $SLURM_JOB_ID | grep $SLURM_JOB_ID | wc -l`

if [ $nb_instances -eq 1 ]
then
        echo "Instance already created:  join ..." $nb_instances $SLURMD_NODENAME  
        apptainer exec instance://$SLURM_JOB_ID $EPOCH_EXE 
else
        apptainer instance list $SLURM_JOB_ID | grep $SLURM_JOB_ID | wc -l
        echo "Instance not created:  create it ..." $nb_instances $SLURMD_NODENAME
        flock -o -x /tmp  apptainer instance start  -B /lustre -B /cvmfs  $CONT $SLURM_JOB_ID
        apptainer exec instance://$SLURM_JOB_ID $EPOCH_EXE
fi
