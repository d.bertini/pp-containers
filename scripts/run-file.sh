#!/bin/bash

export OMP_NUM_THREADS=1
echo $PATH
echo $LD_LIBRARY_PATH

#
# Define container environment to be used
#
export WDIR=/lustre/rz/dbertini/ompi_tests/osu/scripts

# Standard RLX8  container:  gcc v8.5 + openMPI 5.x (system) 
export CONT=/lustre/rz/dbertini/containers/dev/cpu/rlx8_ompi_ucx.sif

# Dev RLX8 container: gcc v13.2 + openMPI 5.x (+latest python 3.12)
#export CONT=/lustre/rz/dbertini/containers/dev/cpu/rlx8_ompi_ucx_gcc13_py312.sif

#
# Apptainer settings
#

export APPTAINER_BINDPATH=/lustre/rz/dbertini/,/cvmfs
export APPTAINER_SHARENS=true
export APPTAINER_CONFIGDIR=/tmp/$USER

# Define the I/O module for openMPI
export OMPI_MCA_io=romio321

# OSU microbenchmarks example
#srun -- $CONT pt2pt/osu_mbw_mr
#srun --export=ALL -- $CONT collective/osu_allreduce -f -i 100000 -x 100000

# EPOCH example
echo "." | srun --export=ALL -- $CONT epoch3d

# WarpX example

#srun --export=ALL -- $CONT warpx_2d $WDIR/warpx_opmd_deck
