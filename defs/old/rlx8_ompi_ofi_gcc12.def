Bootstrap: docker
From: rockylinux:8.7




%labels
    APPLICATION_NAME Plasma Physics Simulation
    AUTHOR_NAME Denis Bertini
    AUTHOR_EMAIL D.Bertini@gsi.de
    YEAR 2023

%help
    Container for Plasma Physics Simulations. 
    It includes latest version of openMPI compiled with:
              - latest OFI (LibFabric) library
              - latest Lustre 2.15 client
    in order to use efficiently on Virgo2:
            - the Infiniband interconnect Fabric.
	    - the Lustre filesystem



%environment
    export OMPI_DIR=/usr/local
    export SINGULARITY_OMPI_DIR=$OMPI_DIR
    export SINGULARITYENV_APPEND_PATH=$OMPI_DIR/bin
    export SINGULAIRTYENV_APPEND_LD_LIBRARY_PATH=$OMPI_DIR/lib
    export PATH=$OMPI_DIR/bin:$PATH
    export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
    export PATH=/usr/local/bin:$PATH
    export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH    
    export PYTHONPATH=/root/.local/lib/python3.6/site-packages/:$PYHTONPATH
    
%post

    ## Prerequisites
    dnf -y update
    dnf install -y dnf-plugins-core
    dnf config-manager --set-enabled powertools
    dnf groupinstall -y 'Development Tools'
    dnf install -y wget git bash hostname gcc gcc-gfortran gcc-c++ make file autoconf automake libtool zlib-devel python3-devel
    dnf install -y libmnl lsof numactl-libs ethtool tcl tk
    dnf install -y gcc-toolset-12  
 
    ## Packages required for OpenMPI and PMIx
    dnf install -y libnl3 libnl3-devel
    dnf install -y libevent libevent-devel
    dnf install -y munge munge-devel
    dnf install -y rdma-core-devel
    dnf install -y hwloc-devel
    dnf install -y libibverbs-devel    


cat <<EOF >/etc/yum.repos.d/lustre-client.repo
[lustre-client]
name=Lustre Client
gpgcheck=0
baseurl=http://downloads.whamcloud.com/public/lustre/lustre-2.15.2/el8.7/client
EOF
yum install -y lustre-client-devel


  #
  # Install python packages
  #
  
  # Update python pip.
  python3 -m pip --no-cache-dir install --upgrade pip
  python3 -m pip --no-cache-dir install setuptools --upgrade

  # Install Data Science packages
  python3 -m pip --no-cache-dir install numpy pandas h5py pyarrow sklearn statsmodels matplotlib seaborn plotly yt
  python3 -m pip --no-cache-dir install networkx    
  # Install package specific to PP 
  python3 -m pip --no-cache-dir install lmfit


   #
   # GCC update to latest version v12.2.0
   #
   
   mkdir /tmp/gcc
   cd /tmp/gcc
   export GCC_VERSION=12.2.0
   echo "Installing gcc version: " $GCC_VERSION		
   wget https://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz
   tar zxf gcc-$GCC_VERSION.tar.gz
   cd gcc-$GCC_VERSION
   ./contrib/download_prerequisites
   cd - # go back to parent dir
   mkdir build && cd build
   ../gcc-$GCC_VERSION/configure --enable-languages=c,c++ --disable-multilib
   make -j$(nproc)   
   make -j$(nproc) install
   rm -rf /tmp/gcc


   # PMIx 3.2.3 -> 4.2.3
   export VPMIX_VERSION=4.2.3
   echo "Installing PMIx version: " $VPMIX_VERSION	
   mkdir -p /tmp/pmix
   cd /tmp/pmix
   wget -c https://github.com/openpmix/openpmix/releases/download/v$VPMIX_VERSION/pmix-$VPMIX_VERSION.tar.gz
   tar xf pmix-$VPMIX_VERSION.tar.gz
   cd pmix-$VPMIX_VERSION
   ./configure --prefix=/usr/local --with-munge=/usr && \
   make -j$(nproc)
   make -j$(nproc) install      
   rm -rf /tmp/pmix


   # libfabric 1.12.1 -> (1.17.1) even more verbose !
   mkdir -p /tmp/libfabric
   cd /tmp/libfabric
   wget -c https://github.com/ofiwg/libfabric/releases/download/v1.17.1/libfabric-1.17.1.tar.bz2
   tar xf libfabric-1.17.1.tar.bz2
   cd libfabric-1.17.1
   ./configure --prefix=/usr/local --disable-debug --enable-mrail=auto --enable-rxd=auto --enable-rxm=auto && \
   make -j$(nproc)
   make -j$(nproc) install   


  #
  ## OpenMPI installation
  #
  

  export OMPI_DIR=/usr/local
  export OMPI_VERSION=4.1.5
  export OMPI_URL="https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-$OMPI_VERSION.tar.bz2"
  echo "Installing OpenMPI version: " $OMPI_VERSION
  mkdir -p /tmp/ompi
  cd /tmp/ompi
  wget -c -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2

  # Compile and install
  cd /tmp/ompi/openmpi-$OMPI_VERSION
  ./configure --prefix=$OMPI_DIR --with-pmix=/usr/local --with-libevent=/usr --with-ompi-pmix-rte --with-orte=no --disable-oshmem --enable-mpirun-prefix-by-default --enable-shared  --without-verbs --with-hwloc --with-ofi=/usr/local/ --with-lustre --with-slurm --enable-mca-no-build=btl-uct --with-cma=no 
  make -j$(nproc)
  make -j$(nproc) install

  # Set env variables so we can compile our applications
  export PATH=$OMPI_DIR/bin:$PATH
  export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
  export MANPATH=$OMPI_DIR/share/man:$MANPATH
  export COMPILER=gfortran

  #
  # Cmake
  #
  
  export CMAKE_DIR=/usr/local
  export CMAKE_VERSION=3.26.1
  echo "Installing CMake version: " $CMAKE_VERSION
  mkdir -p /tmp/cmake
  cd /tmp/cmake
  #wget -q -nc --no-check-certificate -P /tmp/cmake  https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
  curl -OL https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
  /bin/sh /tmp/cmake/cmake-$CMAKE_VERSION-linux-x86_64.sh --prefix=/usr/local --skip-license 
  rm -rf /tmp/cmake
  PATH=/usr/local/bin:$PATH


  # Install HDF5
  export HDF5_VERSION=1.14
  echo "Installing HDF5 version: " $HDF5_VERSION
  cd /tmp
  wget -q -nc --no-check-certificate -L https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.0/src/hdf5-1.14.0.tar.bz2 
  tar -x -f hdf5-1.14.0.tar.bz2
  cd hdf5-1.14.0/
  ./configure --prefix=/usr/local/ --enable-parallel --enable-shared CC=/usr/local/bin/mpicc CXX=/usr/local/bin/mpicxx 
  make -j$(nproc) 
  make -j$(nproc) install 
  rm -rf /tmp/hdf5-1.14.0/


# ADIOS2  
  export ADIOS_VERSION=2.8.3
  echo "Installing ADIOS2 version: " $ADIOS_VERSION
  rm -rf /tmp/adios
  mkdir -p /tmp/adios
  cd /tmp/adios
  git clone https://github.com/ornladios/ADIOS2.git
  cd ADIOS2
  git checkout v$ADIOS_VERSION
  mkdir adios2-build
  cd adios2-build
  cmake ../ -DCMAKE_PREFIX_INSTALL=/usr/local/ -DADIOS2_BUILD_EXAMPLES=OFF -DADIOS2_BUILD_TESTING=OFF -DADIOS2_USE_Fortran=ON -DADIOS2_USE_HDF5=OFF -DCMAKE_CXX_FLAGS=-fPIC
  #cmake ../ -DCMAKE_PREFIX_INSTALL=/usr/local/ -DADIOS2_BUILD_EXAMPLES=OFF -DADIOS2_BUILD_TESTING=OFF -DADIOS2_USE_Fortran=ON  -DCMAKE_CXX_FLAGS=-fPIC 
  make -j$(nproc)
  make -j$(nproc) install  
  rm -rf /tmp/adios
  PATH=/usr/local/bin:$PATH 


  ## Example MPI applications installation - OSU microbenchmarks
  export OSU_VERSION=7.0.1
  echo "Installing OSU Microbenchmarks version: " $OSU_VERSION
  cd /tmp
  wget -c https://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-$OSU_VERSION.tar.gz
  tar xf osu-micro-benchmarks-$OSU_VERSION.tar.gz
  cd osu-micro-benchmarks-$OSU_VERSION/
  echo "Configuring and building OSU Micro-Benchmarks..."
  ./configure --prefix=/usr/local/osu CC=/usr/local/bin/mpicc CXX=/usr/local/bin/mpicxx CFLAGS=-I$(pwd)/util
  make -j$(nproc)
  make -j$(nproc) install
  # copy back all MPI benchmarks to /usr/local
  cp -R /usr/local/osu/libexec/osu-micro-benchmarks/mpi/* /usr/local/bin
  rm -rf /tmp/osu-micro-benchmarks-$OSU_VERSION/  




  #
  # Epoch
  # + python SDF module
  #
  
  export EPOCH_VERSION=4.19.0
  echo " Installing EPOCH version: " $EPOCH_VERSION
  cd /usr/local/
  git clone --recursive https://github.com/Warwick-Plasma/epoch.git
  cd epoch
  git checkout v$EPOCH_VERSION
  cd epoch1d
  sed -i 's/#DEFINES += $(D)PARTICLE_ID$/DEFINES += $(D)PARTICLE_ID/g' Makefile
  make -j$(nproc)
  cp bin/epoch1d /usr/local/bin
  make sdfutils  
  cd ../epoch2d
  sed -i 's/#DEFINES += $(D)PARTICLE_ID$/DEFINES += $(D)PARTICLE_ID/g' Makefile  
  make -j$(nproc)
  cp bin/epoch2d /usr/local/bin
  make sdfutils  
  cd ../epoch3d
  sed -i 's/#DEFINES += $(D)PARTICLE_ID$/DEFINES += $(D)PARTICLE_ID/g' Makefile  
  make -j$(nproc)
  cp bin/epoch3d /usr/local/bin
  make sdfutils
  cd /usr/local/epoch
  cd epoch1d
  make clean
  sed -i 's/INTEGER, PARAMETER :: string_length = 256$/INTEGER, PARAMETER :: string_length = 100000/g' src/constants.F90
  make -j$(nproc)
  cp bin/epoch1d /usr/local/bin/epoch1d_lstr
  cd ../epoch2d
  make clean
  sed -i 's/INTEGER, PARAMETER :: string_length = 256$/INTEGER, PARAMETER :: string_length = 100000/g' src/constants.F90
  make -j$(nproc)
  cp bin/epoch2d /usr/local/bin/epoch2d_lstr
  cd ../epoch3d
  make clean
  sed -i 's/INTEGER, PARAMETER :: string_length = 256$/INTEGER, PARAMETER :: string_length = 100000/g' src/constants.F90
  make -j$(nproc)
  cp bin/epoch3d /usr/local/bin/epoch3d_lstr 


  #
  # WarpX
  # With openMPI + OpenMP + openPMD  only
  #
  export WARPX_VERSION=23.03
  echo "INstalling WarpX version: " $WARPX_VERSION
  mkdir -p /tmp/warp
  cd /tmp/warp
  git clone https://github.com/ECP-WarpX/WarpX.git
  cd WarpX
  git checkout $WARPX_VERSION

  mkdir build_1d
  cd build_1d
  CXX=/usr/local/bin/mpicxx CC=/usr/local/bin/mpicc cmake .. -D WarpX_DIMS=1  -D AMReX_TINY_PROFILE=FALSE -D MPI_C_COMPILER=/usr/local/bin/mpicc -D MPI_CXX_COMPILER=/usr/local/bin/mpicxx -DWarpX_OPENPMD=ON -DCMAKE_INSTALL_PREFIX=/usr/local/
  make -j$(nproc)
  make -j$(nproc) install    
  cd ..
  mkdir build_2d
  cd build_2d 
  CXX=/usr/local/bin/mpicxx CC=/usr/local/bin/mpicc cmake .. -D WarpX_DIMS=2  -D AMReX_TINY_PROFILE=FALSE -D MPI_C_COMPILER=/usr/local/bin/mpicc -D MPI_CXX_COMPILER=/usr/local/bin/mpicxx -DWarpX_OPENPMD=ON -DCMAKE_INSTALL_PREFIX=/usr/local/
  make -j$(nproc)
  make -j$(nproc) install  
  cd ..
  mkdir build_3d
  cd build_3d 
  CXX=/usr/local/bin/mpicxx CC=/usr/local/bin/mpicc cmake .. -D WarpX_DIMS=3  -D AMReX_TINY_PROFILE=FALSE -D MPI_C_COMPILER=/usr/local/bin/mpicc -D MPI_CXX_COMPILER=/usr/local/bin/mpicxx -DWarpX_OPENPMD=ON -DCMAKE_INSTALL_PREFIX=/usr/local/ 
  make -j$(nproc)
  make -j$(nproc) install
  rm -rf /tmp/warp

  # create aliases
  cd /usr/local/bin
  ln -s warpx.1d.MPI.OMP.DP.PDP.OPMD.QED warpx_1d
  ln -s warpx.2d.MPI.OMP.DP.PDP.OPMD.QED warpx_2d
  ln -s warpx.3d.MPI.OMP.DP.PDP.OPMD.QED warpx_3d 


%runscript
#echo "Container will run: /usr/local/osu/libexec/osu-micro-benchmarks/mpi/$*"
#exec /usr/local/osu/libexec/osu-micro-benchmarks/mpi/$*
exec /usr/local/bin/$*

#%runscript
#exec /bin/bash "$@"
#%startscript
#exec /bin/bash "$@"