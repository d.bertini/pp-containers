Bootstrap: docker
From: rockylinux:8.9

%labels
    APPLICATION_NAME Plasma Physics Simulation
    AUTHOR_NAME Denis Bertini
    AUTHOR_EMAIL D.Bertini@gsi.de
    YEAR 2024

%help
    Container for Plasma Physics Simulations. 
    It includes latest version of openMPI compiled with:
              - latest UCX library
              - latest Lustre 2.15 client
    in order to use efficiently on Virgo2:
            - the Infiniband interconnect Fabric.
	    - the Lustre filesystem


%environment
    export OMPI_DIR=/usr/local
    export SINGULARITY_OMPI_DIR=$OMPI_DIR
    export APPTAINERENV_APPEND_PATH=$OMPI_DIR/bin
    export APPTAINERENV_APPEND_LD_LIBRARY_PATH=$OMPI_DIR/lib:$OMPI_DIR/lib64
    export PATH=$OMPI_DIR/bin:$PATH
    export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
    export PATH=/usr/local/bin:$PATH
    export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH
    export PATH=/opt/rocm/bin:$PATH
    export LD_LIBRARY_PATH=/opt/rocm/lib:$LD_LIBRARY_PATH       

    # epoch + openpmd-api
    export PY_API=3.12
    export PYTHONPATH=/root/.local/lib/python$PY_API/site-packages/
    export PYTHONPATH=/usr/local/lib/python$PY_API/site-packages/:$PYTHONPATH
    export PYTHONPATH=/usr/local/lib64/python$PY_API/site-packages/:$PYTHONPATH    
    export PYTHONPATH=/opt/opmd/openPMD-api/build/lib64/python$PY_API/site-packages:$PYTHONPATH    


%files
  /lustre/rz/dbertini/containers/orion/gpu/pytorch-v2.1.2.tar.gz    /opt/pytorch-v2.1.2.tar.gz

%post

   #
   # Base upstream software
   #
    
   ## Packages needed but lustre-client
    dnf install -y epel-release  
    dnf install -y dkms    
    dnf -y update
    
   ## Base rlx8 
    dnf install -y dnf-plugins-core
    dnf config-manager --set-enabled powertools
    dnf groupinstall -y 'Development Tools'
    dnf install -y wget git bash hostname gcc gcc-gfortran gcc-c++ libatomic make file autoconf automake libtool zlib-devel python3-devel
    dnf install -y libmnl lsof numactl-libs ethtool tcl tk sqlite-devel

   ## Add-ons for python's packages
    dnf install -y openssl-devel bzip2-devel libffi-devel
    dnf install alternatives


   ## Packages required for OpenMPI and PMIx
    dnf install -y libnl3 libnl3-devel
    dnf install -y libevent libevent-devel
    dnf install -y munge munge-devel
    dnf install -y rdma-core-devel
    dnf install -y hwloc-devel
    dnf install -y libibverbs-devel    

  
  #
  # ROCM stack
  #


# version
ver=6.0
cat <<EOF >/etc/yum.repos.d/rocm.repo 
[ROCm-$ver]
name=ROCm$ver
baseurl=https://repo.radeon.com/rocm/rhel8/$ver/main
enabled=1
priority=50
gpgcheck=1
gpgkey=https://repo.radeon.com/rocm/rocm.gpg.key
EOF
yum install -y rocm-hip-sdk miopen-hip rccl
    
    export PATH=/opt/rocm/bin:$PATH
    export LD_LIBRARY_PATH=/opt/rocm/lib:/opt/rocm/lib64:$LD_LIBRARY_PATH   


   # Re-install the python
    export PY_API=3.12
    export PY_VERSION=3.12.1
    mkdir -p /tmp/python
    cd /tmp/python
    
    wget https://www.python.org/ftp/python/$PY_VERSION/Python-$PY_VERSION.tgz
    tar -xzf Python-$PY_VERSION.tgz

    cd Python-$PY_VERSION
    ./configure --enable-optimizations --enable-loadable-sqlite-extensions
    #make -j$(nproc) altinstall
    make  altinstall	

   #
   # Install python packages
   #
   alternatives --install /usr/bin/python upy2 /usr/local/bin/python3.12 99
   alternatives --install /usr/bin/python3 upy3 /usr/local/bin/python3.12 99   

   #
   # Install python packages
   # using virtual environment
   #

   # Initiate environment
   python3 -m pip --no-cache-dir install --upgrade pip
   pip install virtualenv

   mkdir /venv
   virtualenv /venv/plasma
   source /venv/plasma/bin/activate

   echo '. /venv/plasma/bin/activate' >> $SINGULARITY_ENVIRONMENT

   ## Update python pip.
     python3 -m pip --no-cache-dir install --upgrade pip
     python3 -m pip --no-cache-dir install setuptools --upgrade

   ## Install Data Science packages
     python3 -m pip --no-cache-dir install ipython jupyterlab   
     python3 -m pip --no-cache-dir install numpy pandas h5py pyarrow scikit-learn  statsmodels matplotlib seaborn plotly yt
     python3 -m pip --no-cache-dir install recommonmark   
     python3 -m pip --no-cache-dir install networkx sphinx sphinx-rtd-theme
     python3 -m pip --no-cache-dir install typing_extensions    

   ## Install package specific to PP 
     python3 -m pip --no-cache-dir install lmfit

    CUSTOM_ENV=/.singularity.d/env/99-zz_custom_env.sh
    cat >$CUSTOM_ENV <<EOF
#!/bin/bash
PS1="[pp_pytorch]\w \$ "
EOF
    chmod 755 $CUSTOM_ENV

cat <<EOF >/etc/yum.repos.d/lustre-client.repo
[lustre-client]
name=Lustre Client
gpgcheck=0
baseurl=http://downloads.whamcloud.com/public/lustre/lustre-2.15.4/el8.9/client
EOF
yum install -y lustre-client-devel


  #
  # Cmake
  #
  
  export CMAKE_DIR=/usr/local
  export CMAKE_VERSION=3.28.1
  echo "Installing CMake version: " $CMAKE_VERSION
  mkdir -p /tmp/cmake
  cd /tmp/cmake
  #wget -q -nc --no-check-certificate -P /root/cmake  https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
  curl -OL https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
  /bin/sh /tmp/cmake/cmake-$CMAKE_VERSION-linux-x86_64.sh --prefix=/usr/local --skip-license 
  rm -rf /tmp/cmake
  PATH=/usr/local/bin:$PATH



   #
   # PMIx 3.2.3 -> 4.2.3
   #
   
   export VPMIX_VERSION=4.2.8
   echo "Installing PMIx version: " $VPMIX_VERSION	
   mkdir -p /tmp/pmix
   cd /tmp/pmix
   wget -c https://github.com/openpmix/openpmix/releases/download/v$VPMIX_VERSION/pmix-$VPMIX_VERSION.tar.gz
   tar xf pmix-$VPMIX_VERSION.tar.gz
   cd pmix-$VPMIX_VERSION
   ./configure --prefix=/usr/local --with-munge=/usr && \
   make -j$(nproc)
   make -j$(nproc) install      
   rm -rf /tmp/pmix

   #
   # UCX
   #
   
   export UCX_VERSION=1.15.0
   echo "Installing UCX version:  " $UCX_VERSION
   cd /
   mkdir -p /var/tmp && wget -q -nc --no-check-certificate -P /var/tmp https://github.com/openucx/ucx/releases/download/v$UCX_VERSION/ucx-$UCX_VERSION.tar.gz
   mkdir -p /var/tmp && tar -x -f /var/tmp/ucx-$UCX_VERSION.tar.gz -C /var/tmp -z
   cd /var/tmp/ucx-$UCX_VERSION &&   ./configure --prefix=/usr/local/ucx --disable-assertions --disable-debug --disable-doxygen-doc --disable-logging --disable-params-check --enable-optimizations  --without-cuda --enable-mt  --with-rocm=/opt/rocm
   make -j$(nproc)
   make -j$(nproc) install
   rm -rf /var/tmp/ucx-$UCX_VERSION /var/tmp/ucx-$UCX_VERSION.tar.gz

  #
  ## OpenMPI installation
  #
  
  export OMPI_DIR=/usr/local
  export OMPI_VERSION=5.0.1
  export OMPI_URL="https://download.open-mpi.org/release/open-mpi/v5.0/openmpi-$OMPI_VERSION.tar.bz2"
  echo "Installing OpenMPI version: " $OMPI_VERSION
  mkdir -p /tmp/ompi
  cd /tmp/ompi
  wget -c -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2

  # Compile and install
  cd /tmp/ompi/openmpi-$OMPI_VERSION
  ./configure --prefix=$OMPI_DIR --with-pmix=/usr/local --with-libevent=/usr --with-ompi-pmix-rte --with-orte=no --disable-oshmem --enable-mpirun-prefix-by-default --enable-shared  --without-verbs --with-hwloc --with-ucx=/usr/local/ucx --with-lustre --with-slurm --enable-mca-no-build=btl-uct 
  make -j$(nproc)
  make -j$(nproc) install

  # Set env variables so we can compile our applications
  export PATH=$OMPI_DIR/bin:$PATH
  export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
  export MANPATH=$OMPI_DIR/share/man:$MANPATH
  export COMPILER=gfortran


  #
  # Install HDF5
  #
  
 export HDF5_VERSION=1.14.3
  echo "Installing HDF5 version: " $HDF5_VERSION
  cd /tmp
  wget -q -nc --no-check-certificate -L https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-$HDF5_VERSION/src/hdf5-$HDF5_VERSION.tar.bz2 
  tar -x -f hdf5-$HDF5_VERSION.tar.bz2
  cd hdf5-$HDF5_VERSION/
  ./configure --prefix=/usr/local/ --enable-parallel --enable-fortran --enable-fortran2003 --enable-shared CC=/usr/local/bin/mpicc CXX=/usr/local/bin/mpicxx FC=/usr/local/bin/mpif90 
  make -j$(nproc) 
  make -j$(nproc) install 
  rm -rf /tmp/hdf5-$HDF5_VERSION/

  #
  # ADIOS2
  #
  
  export ADIOS_VERSION=2.10.0-rc1
  echo "Installing ADIOS2 version: " $ADIOS_VERSION
  rm -rf /tmp/adios
  mkdir -p /tmp/adios
  cd /tmp/adios
  git clone https://github.com/ornladios/ADIOS2.git
  cd ADIOS2
  git checkout v$ADIOS_VERSION
  mkdir adios2-build
  cd adios2-build
  cmake ../ -DCMAKE_PREFIX_INSTALL=/usr/local/ -DADIOS2_BUILD_EXAMPLES=OFF -DADIOS2_BUILD_TESTING=OFF -DADIOS2_USE_Fortran=ON -DADIOS2_USE_HDF5=OFF -DCMAKE_CXX_FLAGS=-fPIC 
  make -j$(nproc)
  make -j$(nproc) install  
  rm -rf /tmp/adios
  PATH=/usr/local/bin:$PATH 


  #
  # openPMD-API
  #
  
  mkdir -p /opt/opmd
  cd /opt/opmd
  git clone  https://github.com/openPMD/openPMD-api.git openPMD-api
  cd /opt/opmd/openPMD-api
  git checkout 0.15.2 
  mkdir -p /opt/opmd/openPMD-api/build
  cd /opt/opmd/openPMD-api/build
  cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DopenPMD_USE_ADIOS2=ON -DopenPMD_USE_ADIOS1=OFF -DopenPMD_USE_PYTHON=ON /opt/opmd/openPMD-api
  cmake --build /opt/opmd/openPMD-api/build --target install -- -j$(nproc)

  # OpenPMD_Viewer 
   python3 -m pip --no-cache-dir install openpmd-viewer 

  #
  #  PyTorch installation from source
  #

  cd /opt
  tar xvfz pytorch-v2.1.2.tar.gz
  cd pytorch-v2.1.2/
  export USE_CUDA=0

  # Add MPI support
  export USE_MPI=1
  export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

  # Add ROCM-GPU support
  export PYTORCH_ROCM_ARCH=gfx908

  # PyTorch specs
  export _GLIBCXX_USE_CXX11_ABI=1	
  export CMAKE_PREFIX_PATH=/root/.local/lib/python3.6/site-packages/:$CMAKE_PREFIX_PATH
  export CMAKE_C_COMPILER=$(which mpicc)
  export CMAKE_CXX_COMPILER=$(which mpicxx)


  # Compile process 
  #python setup.py clean
  MAX_JOBS=5 python setup.py build develop

%runscript
exec /bin/bash "$@"
%startscript
exec /bin/bash "$@"