Bootstrap: docker
From: rockylinux/rockylinux:8.10


%labels
    APPLICATION_NAME Plasma Physics Simulation
    AUTHOR_NAME Denis Bertini
    AUTHOR_EMAIL D.Bertini@gsi.de
    YEAR 2024

%help
    Container for Plasma Physics Simulations. 
    It includes latest version of openMPI compiled with:
              - latest UCX library
              - latest Lustre 2.15 client
    in order to use efficiently on Virgo2:
            - the Infiniband interconnect Fabric.
	    - the Lustre filesystem


%environment
    export OMPI_DIR=/usr/local
    export SINGULARITY_OMPI_DIR=$OMPI_DIR
    export APPTAINERENV_APPEND_PATH=$OMPI_DIR/bin
    export APPTAINERENV_APPEND_LD_LIBRARY_PATH=$OMPI_DIR/lib:$OMPI_DIR/lib64
    export PATH=$OMPI_DIR/bin:$PATH
    export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
    export PATH=/usr/local/bin:$PATH
    export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH
    export PATH=/opt/rocm/bin:$PATH
    export LD_LIBRARY_PATH=/opt/rocm/lib:$LD_LIBRARY_PATH       

    # openMPI specifics
    export OMPI_MCA_io=romio341

    # epoch + openpmd-api
    export PY_API=3.6	
    export PYTHONPATH=/opt/opmd/openPMD-api/build/lib64/python$PY_API/site-packages:$PYTHONPATH
    
%post

   ## Prerequisites
    dnf -y update
    dnf --nogpgcheck install -y  https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm    
    dnf install -y dnf-plugins-core
    dnf config-manager --set-enabled powertools
    dnf groupinstall -y 'Development Tools'
    dnf install -y wget git bash hostname gcc gcc-gfortran gcc-c++ make file autoconf automake libtool zlib-devel 
    dnf install -y libmnl lsof numactl-libs ethtool tcl tk rsync
    dnf install -y perl perl-URI 


   ## Packages needed but lustre-client
    dnf install -y epel-release  
    dnf install -y dkms
    dnf -y upgrade


   ## Packages required for OpenMPI and PMIx
    dnf install -y libnl3 libnl3-devel
    dnf install -y libevent libevent-devel
    dnf install -y munge munge-devel
    dnf install -y rdma-core-devel
    dnf install -y hwloc-devel
    dnf install -y libibverbs-devel    

   ## Add-ons for python's packages
    dnf install -y openssl-devel bzip2-devel libffi-devel
    dnf install -y alternatives

   # Re-install final python3 "only" version
   dnf -y install python3 python3-devel	

   # Set the defaulted python version
   alternatives --set python /usr/bin/python3

   #
   # Install python packages
   # using virtual environment
   #
   
   # Initiate environment
   pip3 install virtualenv

   mkdir /venv
   virtualenv /venv/plasma
   source /venv/plasma/bin/activate

   echo '. /venv/plasma/bin/activate' >> $SINGULARITY_ENVIRONMENT
   
   ## Update python pip.
    python3 -m pip --no-cache-dir install --upgrade pip
    python3 -m pip --no-cache-dir install setuptools --upgrade

   ## Install Data Science packages
    python3 -m pip --no-cache-dir install numpy pandas h5py pyarrow scikit-learn  statsmodels matplotlib seaborn plotly yt

    python3 -m pip --no-cache-dir install recommonmark   
    python3 -m pip --no-cache-dir install networkx sphinx sphinx-rtd-theme  

   ## Install package specific to PP 
    python3 -m pip --no-cache-dir install lmfit
    python3 -m pip --no-cache-dir install dask
    python3 -m pip --no-cache-dir install pyarrow
    CUSTOM_ENV=/.singularity.d/env/99-zz_custom_env.sh
    cat >$CUSTOM_ENV <<EOF
#!/bin/bash
PS1="[pp_amdgpu]\w \$ "
EOF
    chmod 755 $CUSTOM_ENV
  
  #
  # ROCM stack
  #


# version
ver=6.2
cat <<EOF >/etc/yum.repos.d/rocm.repo 
[ROCm-$ver]
name=ROCm$ver
baseurl=https://repo.radeon.com/rocm/rhel8/$ver/main
enabled=1
priority=50
gpgcheck=1
gpgkey=https://repo.radeon.com/rocm/rocm.gpg.key
EOF
yum install -y rocm-hip-sdk

    export PATH=/opt/rocm/bin:$PATH
    export LD_LIBRARY_PATH=/opt/rocm/lib:/opt/rocm/lib64:$LD_LIBRARY_PATH   


cat <<EOF >/etc/yum.repos.d/lustre-client.repo
[lustre-client]
name=Lustre Client
gpgcheck=0
baseurl=http://downloads.whamcloud.com/public/lustre/lustre-2.15.5/el8.10/client
EOF
yum install -y lustre-client lustre-client-devel

   #
   # PMIx 3.2.3 -> 5.0.3
   #
   
   export VPMIX_VERSION=5.0.3
   echo "Installing PMIx version: " $VPMIX_VERSION	
   mkdir -p /tmp/pmix
   cd /tmp/pmix
   wget -c https://github.com/openpmix/openpmix/releases/download/v$VPMIX_VERSION/pmix-$VPMIX_VERSION.tar.gz
   tar xf pmix-$VPMIX_VERSION.tar.gz
   cd pmix-$VPMIX_VERSION
   ./configure --prefix=/usr/local --with-munge=/usr && \
   make -j$(nproc)
   make -j$(nproc) install      
   rm -rf /tmp/pmix

   #
   # UCX
   #
   
   export UCX_VERSION=1.17.0
   echo "Installing UCX version:  " $UCX_VERSION
   cd /
   mkdir -p /var/tmp && wget -q -nc --no-check-certificate -P /var/tmp https://github.com/openucx/ucx/releases/download/v$UCX_VERSION/ucx-$UCX_VERSION.tar.gz
   mkdir -p /var/tmp && tar -x -f /var/tmp/ucx-$UCX_VERSION.tar.gz -C /var/tmp -z
   cd /var/tmp/ucx-$UCX_VERSION &&   ./configure --prefix=/usr/local/ucx --disable-assertions --disable-debug --disable-doxygen-doc --disable-logging --disable-params-check --enable-optimizations  --without-cuda --enable-mt
   make -j$(nproc)
   make -j$(nproc) install
   rm -rf /var/tmp/ucx-$UCX_VERSION /var/tmp/ucx-$UCX_VERSION.tar.gz

  #
  ## OpenMPI installation
  #
  
  export OMPI_DIR=/usr/local
  export OMPI_VERSION=5.0.5
  export OMPI_URL="https://download.open-mpi.org/release/open-mpi/v5.0/openmpi-$OMPI_VERSION.tar.bz2"
  echo "Installing OpenMPI version: " $OMPI_VERSION
  mkdir -p /tmp/ompi
  cd /tmp/ompi
  wget -c -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2

  # Compile and install
  cd /tmp/ompi/openmpi-$OMPI_VERSION
  ./configure --prefix=$OMPI_DIR --with-pmix=/usr/local --with-libevent=/usr --with-ompi-pmix-rte --with-orte=no --disable-oshmem --enable-mpirun-prefix-by-default --enable-shared  --without-verbs --with-hwloc --with-ucx=/usr/local/ucx --with-lustre --with-slurm --enable-mca-no-build=btl-uct 
  make -j$(nproc)
  make -j$(nproc) install

  # Set env variables so we can compile our applications
  export PATH=$OMPI_DIR/bin:$PATH
  export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
  export MANPATH=$OMPI_DIR/share/man:$MANPATH
  export COMPILER=gfortran

  #
  # Cmake
  #
  
  export CMAKE_DIR=/usr/local
  #export CMAKE_VERSION=3.28.1
  export CMAKE_VERSION=3.30.2
  echo "Installing CMake version: " $CMAKE_VERSION
  mkdir -p /tmp/cmake
  cd /tmp/cmake
  #wget -q -nc --no-check-certificate -P /root/cmake  https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
  curl -OL https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
  /bin/sh /tmp/cmake/cmake-$CMAKE_VERSION-linux-x86_64.sh --prefix=/usr/local --skip-license 
  rm -rf /tmp/cmake
  PATH=/usr/local/bin:$PATH

  #
  # Install HDF5
  #
  
  #export HDF5_VERSION=1.14.3
  export HDF5_VERSION=1.14.4-3
  echo "Installing HDF5 version: " $HDF5_VERSION
  cd /tmp
  wget -q -nc --no-check-certificate -L https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.4/src/hdf5-$HDF5_VERSION.tar.gz 
  tar -x -f hdf5-$HDF5_VERSION.tar.gz
  cd hdf5-$HDF5_VERSION/
  ./configure --prefix=/usr/local/ --enable-parallel --enable-fortran --enable-fortran2003 --enable-shared CC=/usr/local/bin/mpicc CXX=/usr/local/bin/mpicxx FC=/usr/local/bin/mpif90 
  make -j$(nproc) 
  make -j$(nproc) install 
  rm -rf /tmp/hdf5-$HDF5_VERSION/

  #
  # ADIOS2
  #
  
  export ADIOS_VERSION=2.10.1
  echo "Installing ADIOS2 version: " $ADIOS_VERSION
  rm -rf /tmp/adios
  mkdir -p /tmp/adios
  cd /tmp/adios
  git clone https://github.com/ornladios/ADIOS2.git
  cd ADIOS2
  git checkout v$ADIOS_VERSION
  mkdir adios2-build
  cd adios2-build
  cmake ../ -DCMAKE_PREFIX_INSTALL=/usr/local/ -DADIOS2_BUILD_EXAMPLES=OFF -DADIOS2_BUILD_TESTING=OFF -DADIOS2_USE_Fortran=ON -DADIOS2_USE_HDF5=OFF -DCMAKE_CXX_FLAGS=-fPIC 
  make -j$(nproc)
  make -j$(nproc) install  
  rm -rf /tmp/adios
  PATH=/usr/local/bin:$PATH 

  

  #
  # openPMD-API
  #
  
  mkdir -p /opt/opmd
  cd /opt/opmd
  git clone  https://github.com/openPMD/openPMD-api.git openPMD-api
  cd /opt/opmd/openPMD-api
  git checkout 0.15.2 
  mkdir -p /opt/opmd/openPMD-api/build
  cd /opt/opmd/openPMD-api/build
  cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DopenPMD_USE_ADIOS2=ON -DopenPMD_USE_ADIOS1=OFF -DopenPMD_USE_PYTHON=ON /opt/opmd/openPMD-api
  cmake --build /opt/opmd/openPMD-api/build --target install -- -j$(nproc)

  # OpenPMD_Viewer 
   python3 -m pip --no-cache-dir install openpmd-viewer 


  #
  # WarpX
  # With openMPI + OpenMP + AMD GPUs
  #
  export WARPX_VERSION=24.08  
  echo "INstalling WarpX version: " $WARPX_VERSION
  
  rm   -rf /tmp/warp
  mkdir -p /tmp/warp
  cd /tmp/warp
  git clone https://github.com/ECP-WarpX/WarpX.git
  cd WarpX
  git checkout $WARPX_VERSION

  # Build all dimensions 1,2,rz,3 together
  CXX=/opt/rocm/bin/hipcc CC=/opt/rocm/bin/amdclang cmake -S . -B build -DWarpX_DIMS="1;2;RZ;3"  -D WarpX_COMPUTE=HIP -D AMReX_AMD_ARCH=gfx908 -D AMReX_TINY_PROFILE=FALSE -D MPI_C_COMPILER=/usr/local/bin/mpicc -D MPI_CXX_COMPILER=/usr/local/bin/mpicxx -DWarpX_OPENPMD=ON -DWarpX_PYTHON=ON -DCMAKE_INSTALL_PREFIX=/usr/local/
  # Install
  cmake --build build -j 10 --target install
  rm -rf /tmp/warp

  # create aliases
  cd /usr/local/bin
  ln -s warpx.1d.MPI.OMP.DP.PDP.OPMD.QED warpx_1d
  ln -s warpx.2d.MPI.OMP.DP.PDP.OPMD.QED warpx_2d
  ln -s warpx.3d.MPI.OMP.DP.PDP.OPMD.QED warpx_3d 
  ln -s warpx.rz.MPI.OMP.DP.PDP.OPMD.QED warpx_rz


%runscript
exec /bin/bash "$@"
%startscript
exec /bin/bash "$@"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               