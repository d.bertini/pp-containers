Bootstrap: docker
From: rockylinux/rockylinux:8.10

%labels
    APPLICATION_NAME Plasma Physics Simulation
    AUTHOR_NAME Denis Bertini
    AUTHOR_EMAIL D.Bertini@gsi.de
    YEAR 2024

%help
    Container for Plasma Physics Simulations. 
    It includes latest version of openMPI compiled with:
              - latest UCX library
              - latest Lustre 2.15 client
    in order to use efficiently on Virgo2:
            - the Infiniband interconnect Fabric.
	    - the Lustre filesystem

%files
    /lustre/rz/dbertini/containers/dev/packages/ci_ompi.tar /opt/ci_ompi.tar
    
%environment
    # Enable gcc-toolset
    source scl_source enable gcc-toolset-13
    
    export OMPI_DIR=/usr/local
    export SINGULARITY_OMPI_DIR=$OMPI_DIR
    export APPTAINERENV_APPEND_PATH=$OMPI_DIR/bin
    export APPTAINERENV_APPEND_LD_LIBRARY_PATH=$OMPI_DIR/lib:$OMPI_DIR/lib64
    export PATH=$OMPI_DIR/bin:$PATH
    export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
    export PATH=/usr/local/bin:$PATH
    export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH

    # openMPI specifics
    export OMPI_MCA_io=romio341

    # epoch + openpmd-api
    export PY_API=3.12
    export PYTHONPATH=/opt/opmd/openPMD-api/build/lib64/python$PY_API/site-packages:$PYTHONPATH
    #export PYTHONPATH=/opt/epoch/python$PY_API:$PYTHONPATH
    #export PYTHONPATH=/opt/epoch/python$PY_API/sdf-1.0-py$PY_API-linux-x86_64.egg/:$PYTHONPATH   

%post

   #
   # Base upstream software
   #
    
   ## Packages needed but lustre-client
    dnf install -y epel-release  
    dnf install -y dkms    
    dnf -y update
    
   ## Base rlx8 
    dnf install -y dnf-plugins-core
    dnf config-manager --set-enabled powertools
    dnf groupinstall -y 'Development Tools'
    dnf install -y wget git bash hostname gcc gcc-gfortran gcc-c++ libatomic make file
    dnf install -y autoconf automake libtool zlib-devel
    dnf install -y libmnl lsof numactl-libs ethtool tcl tk
    dnf install -y gnuplot

   ## Packages required for OpenMPI and PMIx
    dnf install -y libnl3 libnl3-devel
    dnf install -y libevent libevent-devel
    dnf install -y munge munge-devel
    dnf install -y rdma-core-devel
    dnf install -y hwloc-devel
    dnf install -y libibverbs-devel    

   ## Add-ons for python's packages
    dnf install -y openssl-devel bzip2-devel libffi-devel 
    dnf install -y alternatives

   #
   # (Re) Install python package
   #

    export PY_API=3.12
    export PY_VERSION=3.12.1
   # First remove all possible python versionson system 

     dnf remove -y python36 python38 python39 python311 python312

   # Install the only relevant now 
     
     dnf install -y python$PY_API-devel
     dnf install -y python$PY_API-pip
     pip3 install --upgrade pip

   #
   # Set python default
   #
   
   alternatives --install /usr/bin/python upy2 /usr/bin/python$PY_API 99
   alternatives --install /usr/bin/python3 upy3 /usr/bin/python$PY_API 99
   alternatives --install /usr/bin/pip upy4 /usr/bin/pip3 99   
   alternatives --list

   # Check versions of basic packages
   python --version
   pip --version
    

   #
   # Install secondary  python packages
   #         using virtual environment
   #

   # Initiate environment
   python -m pip --no-cache-dir install virtualenv

   mkdir /venv
   virtualenv /venv/plasma
   source /venv/plasma/bin/activate

   echo '. /venv/plasma/bin/activate' >> $SINGULARITY_ENVIRONMENT

   ## Update python pip.
    python3 -m pip --no-cache-dir install --upgrade pip
    python3 -m pip --no-cache-dir install setuptools --upgrade
    
   ## Install Data Science packages
    python3 -m pip --no-cache-dir install setuptools --upgrade	
    python3 -m pip --no-cache-dir install numpy pandas h5py pyarrow scikit-learn  statsmodels matplotlib seaborn plotly yt

    python3 -m pip --no-cache-dir install recommonmark  
    python3 -m pip --no-cache-dir install networkx sphinx sphinx-rtd-theme  

   ## Install package specific to PP 
    python3 -m pip --no-cache-dir install lmfit
    python3 -m pip --no-cache-dir install dask
    python3 -m pip --no-cache-dir install pyarrow

   ## Install modules needed by pytorch
    python3 -m pip --no-cache-dir install typing-extensions

   # Pythons packaging libraris  Needed for WarpX
    python3 -m pip --no-cache-dir install  build packaging setuptools wheel
    python3 -m pip --no-cache-dir install  cmake 
    python3 -m pip --no-cache-dir install  periodictable numba

   CUSTOM_ENV=/.singularity.d/env/99-zz_custom_env.sh
    cat >$CUSTOM_ENV <<EOF
#!/bin/bash
PS1="[pp_container]\w \$ "
EOF
    chmod 755 $CUSTOM_ENV

cat <<EOF >/etc/yum.repos.d/lustre-client.repo
[lustre-client]
name=Lustre Client
gpgcheck=0
baseurl=http://downloads.whamcloud.com/public/lustre/lustre-2.15.5/el8.10/client
EOF
yum install -y lustre-client lustre-client-devel


   ## Prior to compilation
   ## Install and enable gcc-toolset
   dnf install -y gcc-toolset-13
   source scl_source enable gcc-toolset-13


   #
   # PMIx 
   #

   export VPMIX_VERSION=5.0.3
   echo "Installing PMIx version: " $VPMIX_VERSION	
   mkdir -p /tmp/pmix
   cd /tmp/pmix
   wget -c https://github.com/openpmix/openpmix/releases/download/v$VPMIX_VERSION/pmix-$VPMIX_VERSION.tar.gz
   tar xf pmix-$VPMIX_VERSION.tar.gz
   cd pmix-$VPMIX_VERSION
   ./configure --prefix=/usr/local --with-munge=/usr && \
   make -j$(nproc)
   make -j$(nproc) install      
   rm -rf /tmp/pmix

   #
   # UCX
   #

   export UCX_VERSION=1.17.0
   echo "Installing UCX version:  " $UCX_VERSION
   cd /
   mkdir -p /var/tmp && wget -q -nc --no-check-certificate -P /var/tmp https://github.com/openucx/ucx/releases/download/v$UCX_VERSION/ucx-$UCX_VERSION.tar.gz
   mkdir -p /var/tmp && tar -x -f /var/tmp/ucx-$UCX_VERSION.tar.gz -C /var/tmp -z
   cd /var/tmp/ucx-$UCX_VERSION &&   ./configure --prefix=/usr/local/ucx --disable-assertions --disable-debug --disable-doxygen-doc --disable-logging --disable-params-check --enable-optimizations --enable-mt --without-cuda
   make -j$(nproc)
   make -j$(nproc) install
   rm -rf /var/tmp/ucx-$UCX_VERSION /var/tmp/ucx-$UCX_VERSION.tar.gz

   #
   # OpenMPI installation
   #
 
   export OMPI_DIR=/usr/local
   export OMPI_VERSION=5.0.5
   export OMPI_URL="https://download.open-mpi.org/release/open-mpi/v5.0/openmpi-$OMPI_VERSION.tar.bz2"
   echo "Installing OpenMPI version: " $OMPI_VERSION
   mkdir -p /tmp/ompi
   cd /tmp/ompi
   wget -c -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2

   # Compile and install
   cd /tmp/ompi/openmpi-$OMPI_VERSION
   ./configure --prefix=$OMPI_DIR --with-pmix=/usr/local --with-libevent=/usr --with-ompi-pmix-rte --with-orte=no --disable-oshmem --enable-mpirun-prefix-by-default --enable-shared  --without-verbs --with-hwloc --with-ucx=/usr/local/ucx --with-lustre --with-slurm --enable-mca-no-build=btl-uct 
   make -j$(nproc)
   make -j$(nproc) install

   # Set env variables so we can compile our applications
   export PATH=$OMPI_DIR/bin:$PATH
   export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
   export MANPATH=$OMPI_DIR/share/man:$MANPATH
   export COMPILER=gfortran
   export PYTHONPATH=/root/.local/lib/python$PY_API/site-packages:$PYTHONPATH


  # Install MPI based python modules
   export PATH=/usr/local/bin:$PATH
   export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH
   python3 -m pip --no-cache-dir install mpi4py
   python3 -m pip --no-cache-dir install h5py   
   python3 -m pip --no-cache-dir install dask_mpi --upgrade
    

  #
  # Cmake
  #
  
  export CMAKE_DIR=/usr/local
  export CMAKE_VERSION=3.30.2
  echo "Installing CMake version: " $CMAKE_VERSION
  mkdir -p /tmp/cmake
  cd /tmp/cmake
  #wget -q -nc --no-check-certificate -P /root/cmake  https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
  curl -OL https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
  /bin/sh /tmp/cmake/cmake-$CMAKE_VERSION-linux-x86_64.sh --prefix=/usr/local --skip-license 
  rm -rf /tmp/cmake
  PATH=/usr/local/bin:$PATH


  #
  # HDF5
  #

  export HDF5_VERSION=1.14.4-3
  echo "Installing HDF5 version: " $HDF5_VERSION
  cd /tmp
  wget -q -nc --no-check-certificate -L https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.4/src/hdf5-$HDF5_VERSION.tar.gz 
  tar -x -f hdf5-$HDF5_VERSION.tar.gz
  cd hdf5-$HDF5_VERSION/
  ./configure --prefix=/usr/local/ --enable-parallel --enable-fortran --enable-fortran2003 --enable-shared CC=/usr/local/bin/mpicc CXX=/usr/local/bin/mpicxx FC=/usr/local/bin/mpif90 
  make -j$(nproc) 
  make -j$(nproc) install 
  rm -rf /tmp/hdf5-$HDF5_VERSION/
  

  #
  # ADIOS2
  #
  
  export ADIOS_VERSION=2.10.1
  echo "Installing ADIOS2 version: " $ADIOS_VERSION
  rm -rf /tmp/adios
  mkdir -p /tmp/adios
  cd /tmp/adios
  git clone https://github.com/ornladios/ADIOS2.git
  cd ADIOS2
  git checkout v$ADIOS_VERSION
  mkdir adios2-build
  cd adios2-build
  #Need to explicitely set the path for python
  cmake ../ -DCMAKE_PREFIX_INSTALL=/usr/local/ -DADIOS2_BUILD_EXAMPLES=OFF -DADIOS2_BUILD_TESTING=OFF -DADIOS2_USE_Fortran=ON -DADIOS2_USE_HDF5=OFF -DCMAKE_CXX_FLAGS=-fPIC   
  make -j$(nproc)
  make -j$(nproc) install  
  rm -rf /tmp/adios
  PATH=/usr/local/bin:$PATH

  #
  # openPMD-API
  #
  
  mkdir -p /opt/opmd
  cd /opt/opmd
  git clone  https://github.com/openPMD/openPMD-api.git openPMD-api
  cd /opt/opmd/openPMD-api
  git checkout 0.15.2 
  mkdir -p /opt/opmd/openPMD-api/build
  cd /opt/opmd/openPMD-api/build
  cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DopenPMD_USE_ADIOS2=ON -DopenPMD_USE_ADIOS1=OFF -DopenPMD_USE_PYTHON=ON /opt/opmd/openPMD-api
  cmake --build /opt/opmd/openPMD-api/build --target install -- -j$(nproc)

  # OpenPMD_Viewer 
   python3 -m pip --no-cache-dir install openpmd-viewer 


  #
  # OSU microbenchmarks
  #

  export OSU_VERSION=7.4
  echo "Installing OSU Microbenchmarks version: " $OSU_VERSION
  cd /tmp
  wget -c https://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-$OSU_VERSION.tar.gz
  tar xf osu-micro-benchmarks-$OSU_VERSION.tar.gz
  cd osu-micro-benchmarks-$OSU_VERSION/
  echo "Configuring and building OSU Micro-Benchmarks..."
  ./configure --prefix=/usr/local/osu CC=/usr/local/bin/mpicc CXX=/usr/local/bin/mpicxx CFLAGS=-I$(pwd)/util
  make -j$(nproc)
  make -j$(nproc) install
  # copy back all MPI benchmarks to /usr/local
  cp -R /usr/local/osu/libexec/osu-micro-benchmarks/mpi/* /usr/local/bin
  rm -rf /tmp/osu-micro-benchmarks-$OSU_VERSION/  

  #
  # IOR
  #
  
  rm -rf /tmp/io
  mkdir -p /tmp/io
  cd /tmp/io
  git clone https://github.com/hpc/ior.git 
  cd ior/  
  ./bootstrap
  ./configure --with-lustre --with-hfd5 --prefix=/usr/local
  make -j$(nproc)
  make -j$(nproc) install
  rm -rf /tmp/io


  #
  # ci_ompi
  #
  
  export CC=`which gcc`
  export CXX=`which g++`
  export PATH=$PATH:
  cd /opt
  tar xvf ci_ompi.tar
  mkdir build
  cd build
  cmake -DWITH_HDF5=ON -DWITH_FIO=ON -DCMAKE_INSTALL_PREFIX=/usr/local ../ci_ompi
  make -j$(nproc)
  make -j$(nproc) install
  rm -rf /opt/ci_ompi.tar


  #
  # Epoch
  # + python SDF module
  #
  
  export EPOCH_VERSION=4.19.4
  echo " Installing EPOCH version: " $EPOCH_VERSION
  cd /usr/local/
  git clone --recursive https://github.com/Warwick-Plasma/epoch.git
  cd epoch
  git checkout v$EPOCH_VERSION
  sed -i 's/FFLAGS = -O3 -g/FFLAGS = -O3 -g -fPIC/g' SDF/FORTRAN/Makefile
  cd epoch1d
  sed -i 's/FFLAGS = -O3 -g -std=f2003/FFLAGS = -O3 -g -std=f2003 -fPIC/g' Makefile
  sed -i 's/#DEFINES += $(D)PARTICLE_ID$/DEFINES += $(D)PARTICLE_ID/g' Makefile
  make -j$(nproc)
  cp bin/epoch1d /usr/local/bin
  make sdfutils  
  cd ../epoch2d
  sed -i 's/FFLAGS = -O3 -g -std=f2003/FFLAGS = -O3 -g -std=f2003 -fPIC/g' Makefile  
  sed -i 's/#DEFINES += $(D)PARTICLE_ID$/DEFINES += $(D)PARTICLE_ID/g' Makefile  
  make -j$(nproc)
  cp bin/epoch2d /usr/local/bin
  make sdfutils  
  cd ../epoch3d
  sed -i 's/FFLAGS = -O3 -g -std=f2003/FFLAGS = -O3 -g -std=f2003 -fPIC/g' Makefile  
  sed -i 's/#DEFINES += $(D)PARTICLE_ID$/DEFINES += $(D)PARTICLE_ID/g' Makefile  
  make -j$(nproc)
  cp bin/epoch3d /usr/local/bin
  make sdfutils
  cd /usr/local/epoch
  cd epoch1d
  make clean
  sed -i 's/INTEGER, PARAMETER :: string_length = 256$/INTEGER, PARAMETER :: string_length = 100000/g' src/constants.F90
  make -j$(nproc)
  cp bin/epoch1d /usr/local/bin/epoch1d_lstr
  cd ../epoch2d
  make clean
  sed -i 's/INTEGER, PARAMETER :: string_length = 256$/INTEGER, PARAMETER :: string_length = 100000/g' src/constants.F90
  make -j$(nproc)
  cp bin/epoch2d /usr/local/bin/epoch2d_lstr
  cd ../epoch3d
  make clean
  sed -i 's/INTEGER, PARAMETER :: string_length = 256$/INTEGER, PARAMETER :: string_length = 100000/g' src/constants.F90
  make -j$(nproc)
  cp bin/epoch3d /usr/local/bin/epoch3d_lstr
  #mkdir -p /opt/epoch/python3.6/
  #cp -R /root/.local/lib/python3.6/site-packages/sdf* /opt/epoch/python3.6/

  #
  # WarpX
  # With openMPI + OpenMP + openPMD  only
  #
  export WARPX_VERSION=24.09  
  echo "INstalling WarpX version: " $WARPX_VERSION
  
  rm   -rf /tmp/warp
  mkdir -p /tmp/warp
  cd /tmp/warp
  git clone https://github.com/ECP-WarpX/WarpX.git
  cd WarpX
  git checkout $WARPX_VERSION

  # Build all dimensions 1,2,rz,3 together
  CXX=/usr/local/bin/mpicxx CC=/usr/local/bin/mpicc cmake -S . -B build -DWarpX_DIMS="1;2;RZ;3" -D AMReX_TINY_PROFILE=FALSE -D MPI_C_COMPILER=/usr/local/bin/mpicc -D MPI_CXX_COMPILER=/usr/local/bin/mpicxx -DWarpX_OPENPMD=ON -DWarpX_PYTHON=ON -DCMAKE_INSTALL_PREFIX=/usr/local/
  # Install
  cmake --build build -j 10 --target install
  rm -rf /tmp/warp

  # create aliases
  cd /usr/local/bin
  ln -s warpx.1d.MPI.OMP.DP.PDP.OPMD.QED warpx_1d
  ln -s warpx.2d.MPI.OMP.DP.PDP.OPMD.QED warpx_2d
  ln -s warpx.3d.MPI.OMP.DP.PDP.OPMD.QED warpx_3d 
  ln -s warpx.rz.MPI.OMP.DP.PDP.OPMD.QED warpx_rz


%runscript
exec /usr/local/bin/$*


