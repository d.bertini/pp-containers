#!/bin/bash

# Latest GPU environment ( ROCM 6.2 ) + latest Warpx software stack
export CONT=/lustre/rz/dbertini/containers/dev/gpu/rlx8_rocm-6.2_warpx.sif
export WDIR=/lustre/rz/dbertini/gpu/warpx

# Define Appatiner settings
export APPTAINER_BINDPATH=/lustre/rz/dbertini/,/cvmfs
export APPTAINER_SHARENS=true
export APPTAINER_CONFIGDIR=/tmp/$USER
# Define openMPI I/O module 
export OMPI_MCA_io=romio321

srun --export=ALL -- $CONT warpx_2d  $WDIR/scripts/inputs/warpx_opmd_deck
