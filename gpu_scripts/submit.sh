
sbatch --nodes 1 --ntasks-per-node 2 --cpus-per-task 8 --gres=gpu:2 --mem-per-gpu 48G --no-requeue --job-name warpx  --mail-type ALL --mail-user d.bertini@gsi.de --partition gpu --time 0-8:00:00 -D ./ -o %j.out.log -e %j.err.log    ./run-file.sh
